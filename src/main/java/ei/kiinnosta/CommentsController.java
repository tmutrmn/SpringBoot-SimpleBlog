package ei.kiinnosta;

//import java.util.*;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CommentsController {

    //List<Post> _posts;

    @Autowired
    CommentsRepository comments;

    @PostConstruct
    public void init() {
        // called when spring has created the bean
        System.out.println("CommentsController init");
        // save some dummy data

        comments.save(new Comment(1, "commenter 1", "comment 1 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
        comments.save(new Comment(1, "commenter 2", "comment 2 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
        comments.save(new Comment(1, "commenter 3", "comment 3 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));


        comments.save(new Comment(2, "commenter 1", "comment 1 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
        comments.save(new Comment(2, "commenter 2", "comment 2 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));

        comments.save(new Comment(3, "commenter 2", "comment 2 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
    }


    @RequestMapping(value = "/posts/{postId}/comments", method = RequestMethod.GET)
    public Iterable<Comment> fetchComments(@PathVariable long postId) {
        return comments.findByPostId(postId);
    }


		@CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(value = "/posts/{postId}/comments",  method = RequestMethod.POST)
    public Comment saveCustomer(@RequestBody Comment post) {
        // post.setPubDate();       // needed if setPubDate isnt used on dummy contructor
        comments.save(post);
        return post;
		}


    @RequestMapping(value = "/posts/{postId}/comments/{commentId}", method = RequestMethod.GET)
    public Comment fetchPost(@PathVariable long postId, @PathVariable long commentId) {
        // no need to use postId
        return comments.findOne(commentId);
    }

/*
    @RequestMapping(value = "/posts/{postId}/comments/{commentId}", method = RequestMethod.DELETE)
    public Post deletePost(@PathVariable long postId) {
        Post comments = comments.findOne(postId);
        comments.delete(postId);
        return comments;
    }
*/
}
