package ei.kiinnosta;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;



@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


/*
    public interface HtmlHelper {
        public String createH1(String title);
    }


    @Service
    class HtmlHelperImpl implements HtmlHelper {
        public String createH1(String title) {
            String str = "<h1>" + title + "</h1>";
            return str;
        }
    }



    @Bean
    public CommandLineRunner mrBean() {
        class MyCommanLineRunner implements CommandLineRunner {
            public void run(String... strings) throws Exception {
                System.out.println("java kakkakikkare from bean");
            }
        }
        return new MyCommanLineRunner();
    }

    @Bean
    public CommandLineRunner mrBeanToo() {
        return (args) -> System.out.println("lambda java kakkakikkare from bean");
    }


    @Component
    class MyCommanLineRunnerToo implements CommandLineRunner {
        public void run(String... strings) throws Exception {
            System.out.println("java kakkakikkare from component bean");
        }
    }


    @Component
    class MyCommanLineRunnerThree implements CommandLineRunner {
        @Autowired
        HtmlHelper hp;

        public void run(String... strings) throws Exception {
            System.out.println(hp.createH1("hello"));
        }
		}
*/
}
