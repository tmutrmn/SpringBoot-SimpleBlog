package ei.kiinnosta;


import java.util.Calendar;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long postId;
    private String name;
    private String text;
    private Timestamp pubDate;      // timestamp vs. string?


    public Comment() {     // stupid java needs a dummy contructor...
        this.setPubDate();          // miksi?
    }                   
    
    
    public Comment(long postId, String name, String text) {
        this.setPostId(postId);
        this.setName(name);
        this.setText(text);
        this.setPubDate();          // miksi?
    }

   

    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return this.id;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }
    public long getPostId() {
        return this.postId;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

    public void setText(String text) {
        this.text = text;
    }
    public String getText() {
        return this.text;
    }

    public void setPubDate() {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());     
        this.pubDate = currentTimestamp;        // java.sql.Date currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
    }
    public Timestamp getPubDate() {
        return this.pubDate;
    }

    
    
}