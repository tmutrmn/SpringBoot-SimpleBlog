package ei.kiinnosta;


import java.util.Calendar;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String content;
    private String pubDate;      // timestamp vs. string?



    /*
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date();
    String frmtdDate = dateFormat.format(date);
    System.out.println("frmtdDate: " + frmtdDate); 
    if you are trying to fit the date into some DB statement, you should not do it in the form of text, 
    instead use one of the JDBC setters that utilize java.sql.Date or java.sql.Timestamp
    */

    public Post() {     // stupid java needs a dummy contructor...
        /*this.setTitle("");
        this.setContent("");*/
        this.setPubDate();          // miksi?
    }                   
    
    
    public Post(String title, String content) {
        this.setTitle(title);
        this.setContent(content);
        this.setPubDate();          // miksi?
    }

   

    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return this.id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return this.title;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public String getContent() {
        return this.content;
    }

    public void setPubDate() {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
        this.pubDate = currentTimestamp.toString();        // java.sql.Date currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
    }
    public String getPubDate() {
        return this.pubDate;
    }

    
    
}