package ei.kiinnosta;


import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface CommentsRepository extends CrudRepository<Comment, Long> {
    List<Comment> findByPostId(long postId);
}