package ei.kiinnosta;


import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PostsController {

    //List<Post> _posts;

    @Autowired
    PostsRepository posts;


    @PostConstruct
    public void init() {
        // called when spring has created the bean
        System.out.println("PostsController init");
        // save same dummy data
        posts.save(new Post("title 1", "content 1 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
        posts.save(new Post("title 2", "content 2 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
        posts.save(new Post("title 3", "content 3 plöuitrt6jvh ektrygavf gsdhjö sdrt,lsykjmoöi xzfyukj dfujt kytr"));
    }


    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public Iterable<Post> fetchPosts() {
        return posts.findAll();
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(value = "/posts",  method = RequestMethod.POST)
    public Post saveCustomer(@RequestBody Post post) {
        // post.setPubDate();       // needed if setPubDate isnt used on dummy contructor
        posts.save(post);
        return post;
    }


    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.GET)
    public Post fetchPost(@PathVariable long postId) {
        return posts.findOne(postId);
    }


    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public Post deletePost(@PathVariable long postId) {
        Post post = posts.findOne(postId);
        posts.delete(postId);
        return post;
    }

}
