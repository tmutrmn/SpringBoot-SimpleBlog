"use strict";

import React from "react";
import { Link } from "react-router";
import CommentForm from "./commentform.js";
import CommentList from "./commentlist.js";


class Post extends React.Component {
	constructor(props) {
		super(props);
		// console.log(this.props.routeParams.id);
		this.state = {
			id: this.props.routeParams.id,
			data: {}
    }
  }

	componentWillMount() {
		let _this = this;

		const headers = new Headers();
		const conf = {
			method: 'GET',
			headers: headers,
			mode: 'cors',		// same-origin, no-cors, cors - use cors!	(when the options mode property is set to no-cors the request header values are immutable.)
			credentials: 'omit',
			cache: 'default'
		};
		const apiRequest = new Request('http://localhost:3030/posts/' + this.state.id + '', conf);

		fetch(apiRequest).then((response) => {
			let contentType = response.headers.get("content-type")
			if(contentType && contentType.includes("application/json")) {
				return response.json()
			}
			throw new TypeError("Error, returned data was not JSON!")
		})
		.then((json) => {
			// console.log(json)
			_this.setState({
				data: json
			})
		})
		.catch((error) => {
			console.log(error)
		})
  }

  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}


	render() {		// shouild use moment.js for formatting date data...
		return(
			<div>
				<h1>{this.state.data.title}</h1>
				<h4>{this.state.data.pubDate}</h4>
				<p>{this.state.data.content}</p>
				<CommentList key={this.state.id} id={this.state.id} />
				<br />
				<Link to='/'>&laquo; back</Link>
			</div>
		)
  }
}

export default Post;
