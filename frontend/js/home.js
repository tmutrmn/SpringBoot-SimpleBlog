"use strict";

import React from "react";
import PostList from "./postlist.js";


const Home = React.createClass({
  render(){
    return (
			<div>
				<h1>My Blog</h1>
				<h3>Simple Blog app done with SpringBoot Jaava-backend and React Frontend</h3>
				<PostList />
			</div>
    );
  }
});

export default Home;
