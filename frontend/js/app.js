"use strict";

/*Hello World!*/
import React from "react";
import { render } from "react-dom";
import { Router, Route, Link, browserHistory } from "react-router";

import Home from "./home.js";
import Post from "./post.js";


const Hello = () => <h1>Hello World!</h1>

const NotFound = () => (
	<h1>404.. This page is not found!</h1>
)

const Address = () => (
	<h2>Address</h2>
)

const About = (props) => (
	<div>
		<h1>About</h1>
		<p>Really simple blog posting / commenting app <br /><br />
		Jaava/SpringBoot REST backend with Derby DB<br />
		React (Reactpack) frontend.<br />
		</p>
		<p>&copy; tmutrmn. 2017</p>
		<Link to='/'>&laquo; back</Link>
	</div>
)


const Routes = (
	<Router history={browserHistory}>
		<Route name="home" path="/" component={Home} />
		<Route name="post" path="/posts/:id" component={Post} />
		<Route name="about" path='/about(/:name)' component={About} />
		<Route path='*' component={NotFound} />
	</Router>
);



render(
	Routes,
  document.getElementById("react")
)
