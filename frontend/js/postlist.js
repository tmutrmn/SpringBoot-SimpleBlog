"use strict";

import React from "react";
import { Link } from 'react-router';
import PostList_Item from "./postlist_item.js";
import PostForm from "./postform.js";


class PostList extends React.Component {
	constructor() {
		super();
		this.state = { items:[] };
		this.handlePostSubmit = this.handlePostSubmit.bind(this);
  }

	componentWillMount() {
		let _this = this;

		const headers = new Headers();
		const conf = {
			method: 'GET',
			headers: headers,
			mode: 'cors',		// same-origin, no-cors, cors - use cors!	(when the options mode property is set to no-cors the request header values are immutable.)
			credentials: 'omit',
			cache: 'default'
		};
		const apiRequest = new Request('http://localhost:3030/posts', conf);

		fetch(apiRequest).then((response) => {
			let contentType = response.headers.get("content-type")
			if(contentType && contentType.includes("application/json")) {
				return response.json()
			}
			throw new TypeError("Error, returned data was not JSON!")
		})
		.then((json) => {
			// console.log(json)
			_this.setState({
				items: json
			})
		})
		.catch((error) => {
			console.log(error)
		})
  }

  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}


	handlePostSubmit(post) {
		let _posts = this.state.items;
		let _this = this;

		/*var csrfCookie = getCsrfCookie();
		alert(csrfCookie);

		function getCsrfCookie() {
			var ret = "";
			var tab = document.cookie.split(";");

			if (tab.length > 0) {
				var tab1 = tab[0].split("=");
				if (tab1.length > 1) {
					ret = tab1[1];
				}
			}
			return ret;
		}*/


		const headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'cache-control': 'no-cache' });
		const conf = {
			method: 'POST',
			headers: headers,	// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers
			mode: 'cors',		// same-origin, no-cors, cors - use cors!	(when the options mode property is set to no-cors the request header values are immutable.)
			credentials: 'omit',		// omit, same-origin, include		use omit...
			body: JSON.stringify(post)
		};
		const apiRequest = new Request('http://localhost:3030/posts', conf);

		fetch(apiRequest).then((response) => {
			let contentType = response.headers.get("content-type")
			if(contentType && contentType.includes("application/json")) {
				return response.json()
			}
			throw new TypeError("Error, returned data was not JSON!")
		})
		.then((json) => {
			post.id = json.id;		// set the id
			_posts.push(post);
			_this.setState({
				items: _posts
			})
		})
		.catch((error) => {
			console.log(error)
		})
	}


	render() {
		return(
			<div>
				<ul>
					{
						this.state.items.length ? this.state.items.map(item => <PostList_Item key={item.id} id={item.id} title={item.title} />) : <li>Loading...</li>
					}
				</ul>
				<PostForm key='post-form' onPostSubmit={this.handlePostSubmit} />
				<p><Link to={'/about'}>About</Link></p>
			</div>
		)
  }
}

export default PostList;
