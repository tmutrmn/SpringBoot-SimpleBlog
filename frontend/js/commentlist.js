"use strict";

import React from "react";
import CommentForm from "./commentform.js";


class CommentList extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			comments:[]
		};
		this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
  }


	componentWillMount() {
		let _this = this;

		const headers = new Headers();
		const conf = {
			method: 'GET',
			headers: headers,
			mode: 'cors',		// same-origin, no-cors, cors - use cors!	(when the options mode property is set to no-cors the request header values are immutable.)
			credentials: 'omit',
			cache: 'default'
		};
		const apiRequest = new Request('http://localhost:3030/posts/' + this.props.id + '/comments', conf);

		fetch(apiRequest).then((response) => {
			let contentType = response.headers.get("content-type")
			if(contentType && contentType.includes("application/json")) {
				return response.json()
			}
			throw new TypeError("Error, returned data was not JSON!")
		})
		.then((json) => {
			// console.log(json)
			_this.setState({
				comments: json
			})
		})
		.catch((error) => {
			console.log(error)
		})
  }


  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}


	handleCommentSubmit(comment) {
		let _comments = this.state.comments;
		// console.log(comment);

		const headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'cache-control': 'no-cache' });
		const conf = {
			method: 'POST',
			headers: headers,	// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Headers
			mode: 'cors',		// same-origin, no-cors, cors - use cors!	(when the options mode property is set to no-cors the request header values are immutable.)
			credentials: 'omit',		// omit, same-origin, include		use omit...
			body: JSON.stringify(comment)
		};
		const apiRequest = new Request('http://localhost:3030/posts/' + comment.postId + '/comments', conf);

		fetch(apiRequest).then((response) => {
			let contentType = response.headers.get("content-type")
			if(contentType && contentType.includes("application/json")) {
				return response.json()
			}
			throw new TypeError("Error, returned data was not JSON!")
		})
		.then((json) => {
			comment.id = json.id;		// ass we do
			_comments.push(comment);
			this.setState({
				comments: _comments
			})
		})
		.catch((error) => {
			console.log(error)
		})
	}

	render() {
		return(
			<div>
				<h3>comments:</h3>
				<ul>
					{
						this.state.comments.length ?
						this.state.comments.map(comment=><li key={comment.id}><hr /><p><strong>{comment.name}</strong> said on {comment.date}: </p><p>{comment.text}</p></li>)
						: <li>No comments. Be first to comment...</li>
					}
				</ul>
				<CommentForm key='comment-form' id={this.props.id} onCommentSubmit={this.handleCommentSubmit} />
			</div>
		)
  }
}

export default CommentList;
