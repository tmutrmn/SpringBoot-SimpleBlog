# SpringBoot SimpleBlog

### Backend
1. open cmd
2. git clone project from [my git](https://gitlab.com/tmutrmn/SpringBoot-SimpleBlog)
3. cd to project folder (obviously you have jaaava and maven already installed)
4. mvn compile
5. mvn spring-boot:run (starts backend server)

### Frontend
1. open another cmd
2. cd to project folder
3. cd frontend (obviously you have nodejs and npm already installed)
4. npm i
5. npm start (starts frontend)

*  point your browser to [localhost:8080](http://localhost:8080/)

*  profit?
